﻿
using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using IpcProcess;

namespace TestConsole
{
	class Program
	{
		public static void Main(string[] args)
		{
			var key = SchemaRepository.Register("test", Assembly.GetExecutingAssembly());
			var p = Process.AcceptIPC(key.ID, args);
			p.OnRecieved += (d) => {
				
			};
			
			
			Application.Run();
		}
	}
}