﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
namespace IpcProcess
{
	public class ProcessInfo
	{
		public event Action<byte[]> OnRecieved;

		internal void RaiseOnRecieved(byte[] data) {
			var handler = OnRecieved;
			if(handler != null)
				handler(data);
		}
		
		public IPCWriter Writer {
			get;
			set;
		}
		public IPCReader Reader { get; set; }

		public string ExecutablePath {
			get;
			set;
		}

		public Guid ID {
			get;
			set;
		}
		
		public Dictionary<string, string> Args { get; set; }
		
	}
}

