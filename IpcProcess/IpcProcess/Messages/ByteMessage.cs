﻿
using System;

namespace IpcProcess.Messages
{
	public class ByteMessage : IMessage
	{
		public byte Value { get; set; }
		
		public ByteMessage(byte value)
		{
			this.Value = value;
			
		}
		#region IMessage implementation

		public byte[] Serialize()
		{
			return new byte[] {Value};
		}

		public void Deserialize(byte[] raw)
		{
			Value = raw[0];
		}

		#endregion

		
	}
}