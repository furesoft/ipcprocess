﻿
using System;

namespace IpcProcess.Messages
{
	public class Int64Message : IMessage
	{
		public long Value { get; set; }

		#region IMessage implementation

		public byte[] Serialize()
		{
			return BitConverter.GetBytes(Value);
		}

		public void Deserialize(byte[] raw)
		{
			Value = BitConverter.ToInt64(raw, 0);
		}

		#endregion
	}
}