﻿
using System;
using System.Collections;
using System.IO;
namespace IpcProcess.Messages
{
	public class ArrayMessage : IMessage
	{
		public int Count {
			get;
			set;
		}

		public Type Type {
			get;
			set;
		}

		public Array Array {
			get;
			set;
		}

		public ArrayMessage(object arr)
		{
			this.Array = (Array)arr;
			Type = Array.GetValue(0).GetType();
			Count = Array.GetLength(0);
		}

		public ArrayMessage()
		{
		}

		#region IMessage implementation
		public byte[] Serialize()
		{
			var ms = new MemoryStream();
			var bw = new BinaryWriter(ms);
			bw.Write(Type.FullName);
			bw.Write(Count);
			var m = bw.GetType().GetMethod("Write", new[] {
				Type
			});
			for (int i = 0; i < Count; i++) {
				m.Invoke(bw, new[] {
					Array.GetValue(i)
				});
			}
			return ms.ToArray();
		}

		public void Deserialize(byte[] raw)
		{
			var br = new BinaryReader(new MemoryStream(raw));
			var res = new ArrayList();
			var t = br.ReadString();
			this.Type = System.Type.GetType(t);
			this.Count = br.ReadInt32();
			for (int i = 0; i < Count; i++) {
				var m = br.GetType().GetMethod("Read" + Type.Name);
				var v = m.Invoke(br, null);
				res.Add(v);
			}
			this.Array = res.ToArray();
			
			br.Close();
		}
	#endregion
	}
}

