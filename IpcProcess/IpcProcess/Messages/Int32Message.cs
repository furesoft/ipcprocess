﻿
using System;

namespace IpcProcess.Messages
{
	public class Int32Message : IMessage
	{
		public int Value { get; set; }

		#region IMessage implementation

		public byte[] Serialize()
		{
			return BitConverter.GetBytes(Value);
		}

		public void Deserialize(byte[] raw)
		{
			Value = BitConverter.ToInt32(raw, 0);
		}

		#endregion
	}
}