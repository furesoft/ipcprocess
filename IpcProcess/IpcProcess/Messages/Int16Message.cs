﻿
using System;

namespace IpcProcess.Messages
{
	public class Int16Message : IMessage
	{
		public short Value { get; set; }

		#region IMessage implementation

		public byte[] Serialize()
		{
			return BitConverter.GetBytes(Value);
		}

		public void Deserialize(byte[] raw)
		{
			Value = BitConverter.ToInt16(raw, 0);
		}

		#endregion
	}
}