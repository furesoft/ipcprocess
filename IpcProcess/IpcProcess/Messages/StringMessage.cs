﻿
using System;
using System.Text;

namespace IpcProcess.Messages
{
	public class StringMessage : IMessage
	{
		public string Value { get; set; }
		
		public StringMessage(string value)
		{
			this.Value = value;
		}
		
		#region IMessage implementation

	public byte[] Serialize()
	{
		return Encoding.ASCII.GetBytes(Value);
	}

	public void Deserialize(byte[] raw)
	{
		Value = Encoding.ASCII.GetString(raw);
	}

	#endregion

	}
}