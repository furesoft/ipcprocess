﻿
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace IpcProcess.Messages
{
	[Serializable]
	public class DynamicMessage : DynamicObject, IMessage
	{
		private Dictionary<string, object> _properties = new Dictionary<string, object>();

		#region IMessage implementation

		public byte[] Serialize()
		{
			var bf = new BinaryFormatter();
			var ms = new MemoryStream();
			bf.Serialize(ms, _properties);
			
			return ms.ToArray();
		}

		public void Deserialize(byte[] raw)
		{
			var bf = new BinaryFormatter();
			_properties = (Dictionary<string, object>)bf.Deserialize(new MemoryStream(raw));
		}

		#endregion
		
		public override IEnumerable<string> GetDynamicMemberNames()
		{
			return _properties.Keys;
		}
		
		public override bool TryGetMember(GetMemberBinder binder, out object result)
		{
			if (!_properties.ContainsKey(binder.Name)) {
				result = null;
				return false;
			}
			result = _properties[binder.Name];
			return true;
		}
		public override bool TrySetMember(SetMemberBinder binder, object value)
		{
			if (_properties.ContainsKey(binder.Name)) {
				_properties[binder.Name] = value;
			} else {
				_properties.Add(binder.Name, value);
			}
			return true;
		}
	}
}