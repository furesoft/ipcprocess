﻿
using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

namespace IpcProcess
{
	public class IPCReader : BinaryReader
	{
		public IPCReader(Stream strm)
			: base(strm)
		{
			
		}
		
		public Array ReadArray<T>()
		{
			var t = typeof(T);
			var count = ReadInt32();
			var res = new ArrayList();
			
			var m = GetType().GetMethod("Read" + t.Name, new Type[0]);
			for (int i = 0; i < count; i++) {
				var r = m.Invoke(this, new object[0]);
				res.Add(r);
			}
			
			return res.ToArray();
		}
		public T ReadMessage<T>() where T : IMessage, new()
		{
			var msg = new T();
			var count = ReadInt32();
			msg.Deserialize(ReadBytes(count));
			
			return msg;
		}
		public T ReadStruct<T>()
		{
			var count = ReadInt32();
			var raw = ReadBytes(count);
			
			byte[] tmp = new byte[raw.Length];
			Array.Copy(raw, 0, tmp, 0, raw.Length); 

			GCHandle structHandle = GCHandle.Alloc(tmp, GCHandleType.Pinned);
			object structure = Marshal.PtrToStructure(structHandle.AddrOfPinnedObject(), typeof(T));
			structHandle.Free();
			
			return (T)structure;
		}
	}
}