﻿using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace IpcProcess
{
	public class RegistrationKeyConverter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
		{
			return sourceType == typeof(string);
		}

		public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
		{
			var ms = new MemoryStream(Convert.FromBase64String((string)value));
			var bf = new BinaryFormatter();
			return bf.Deserialize(ms);
		}

		public override bool CanConvertTo(ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType == typeof(string);
		}

		public override object ConvertTo(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value, Type destinationType)
		{
			var ms = new MemoryStream();
			var bf = new BinaryFormatter();
			bf.Serialize(ms, value);
			return Convert.ToBase64String(ms.ToArray());
		}
	}
}

