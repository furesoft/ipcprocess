﻿
using System;
using System.Collections.Generic;
using IpcProcess.Messages;

namespace IpcProcess
{
	public static class WellKnownMessages
	{
		private static Dictionary<short, Type> _messages;
		
		static WellKnownMessages()
		{
			_messages = new Dictionary<short, Type> {
				{ (short)PrimitiveMessages.Int16, typeof(Int16Message) },
				{ (short)PrimitiveMessages.Int32, typeof(Int32Message) },
				{ (short)PrimitiveMessages.Int64, typeof(Int64Message) },
				{ (short)PrimitiveMessages.Byte, typeof(ByteMessage) },
				{ (short)PrimitiveMessages.String, typeof(StringMessage) },
				{ (short)PrimitiveMessages.Array, typeof(ArrayMessage) },
				{ (short)PrimitiveMessages.Dynamic, typeof(DynamicMessage) },
			};
		}
		
		public static void AddMessage(Type msgType)
		{
			short last = (short)_messages.Count;
			_messages.Add(last, msgType);
		}
		
		public enum PrimitiveMessages : short
		{
			Int16 = 0,
			Int32 = 1,
			Int64 = 2,
			String = 3,
			Byte = 4,
			Array = 5,
			Dynamic = 6
		}
	}
}