﻿
using System;

namespace IpcProcess
{
	public interface IMessage
	{
		byte[] Serialize();
		void Deserialize(byte[] raw);
	}
}