﻿using System;
using System.IO;
using System.Reflection;
using System.Xaml;

[assembly: System.Windows.Markup.XmlnsDefinition("http://schemas.microsoft.com/winfx/2006/xaml", "IpcProcess")]
	
namespace IpcProcess
{
	public static class SchemaRepository
	{
		public static string RepositoryFile = string.Format("{0}\\ipc_repository.xaml", Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData));
		public static SchemaDictionary _storage = new SchemaDictionary();
		
		public static RegistrationKey GetKey(string schema)
		{
			if (_storage.ContainsKey(schema)) {
				return _storage[schema];
			}
			return null;
		}
		
		static SchemaRepository()
		{
			Load();
		}
		
		public static void Load()
		{
			if (File.Exists(RepositoryFile)) {
				_storage = (SchemaDictionary)XamlServices.Load(RepositoryFile);
			}
		}
		
		public static void Save()
		{
			XamlServices.Save(RepositoryFile, _storage);
		}
		
		public static RegistrationKey Register(string schema, Assembly ass)
		{
			return Register(schema, new FileInfo(ass.Location));
		}
		public static RegistrationKey Register(string schema, FileInfo executable)
		{
			var rk = new RegistrationKey();
			rk.ID = Guid.NewGuid();
			rk.Schema = schema;
			rk.Path = executable;
			
			if (_storage.ContainsKey(schema)) {
				return _storage[schema];
			}
			   
			_storage.Add(schema, rk);
			
			Save();
			
			return rk;
		}
		public static void DeRegister(string schema, RegistrationKey key)
		{
			if (_storage.ContainsKey(schema)) {
				if (_storage[schema] == key) {
					_storage.Remove(schema);
				}
			}
			
			Save();
		}
	}
}