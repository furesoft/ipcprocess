﻿
using System;
using System.IO;

namespace IpcProcess
{
	public static class Process
	{
		public static ProcessInfo AcceptIPC(Guid id, string[] args)
		{
			var ownCom = new MemoryMappedFileCommunicator(id.ToString(), 5000);
			ownCom.WritePosition = 2500;
			ownCom.ReadPosition = 0;
			
			var pi = new ProcessInfo();

			pi.Writer = new IPCWriter(ownCom.Write);
			pi.ID = id;
			
			ownCom.DataReceived += (s, e) => {
				pi.RaiseOnRecieved(e.Data);
			};
			
			ownCom.StartReader();	
			return pi;
		}
		
		public static ProcessInfo Start(string URI)
		{
			var pi = new ProcessInfo();
			var uri = new Uri(URI);
			var rk = SchemaRepository.GetKey(uri.Scheme);
			
			// redirect like on accept
			var ownCom = new MemoryMappedFileCommunicator(rk.ID.ToString(), 5000);
			ownCom.WritePosition = 0;
			ownCom.ReadPosition = 2500;
			ownCom.DataReceived += (s, e) => {
				pi.RaiseOnRecieved(e.Data);
			};
			pi.Writer = new IPCWriter(ownCom.Write);
			ownCom.StartReader();
			
			//initialize info
			pi.ID = rk.ID;
			//register ipc routines
			//start process of schema with key of caller: id://args
			var pif = System.Diagnostics.Process.Start(rk.Path.ToString(), rk.ID + "://" + uri.ToString().Remove(0, (rk.Schema + "://").Length));
			
			return pi;
		}
	}
}