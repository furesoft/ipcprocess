﻿using System;
using System.ComponentModel;
using System.IO;

namespace IpcProcess
{
	[TypeConverter(typeof(RegistrationKeyConverter))]
	[Serializable]
	public class RegistrationKey
	{
		public string Schema { get; set; }
		public FileInfo Path { get; set; }
		public Guid ID { get; set; }
	}
}