﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using IpcProcess.Messages;
namespace IpcProcess
{
	public class IPCWriter
	{
		readonly Action<byte[]> writeFunc;

		internal IPCWriter(Action<byte[]> write)
		{
			this.writeFunc = write;
		}

		public void Write(byte[] raw)
		{
			writeFunc(raw);
		}

		public void Write(short val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(int val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(long val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(double val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(float val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(bool val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(char val)
		{
			Write(BitConverter.GetBytes(val));
		}

		public void Write(string val)
		{
			Write(Encoding.ASCII.GetBytes(val));
		}
		public void Write<T>(T[] a)
		{
			var t = a.GetValue(0).GetType();
			var count = a.GetLength(0);
			Write(count);
			
			var m = GetType().GetMethod("Write", new []{ t });
			
			foreach (var e in a) {
				m.Invoke(this, new object[]{ e });
			}
		}
		public void Write(IMessage msg)
		{
			var arr = msg.Serialize();
			
			Write(arr.Length);
			Write(arr);
		}
		public void Write<T>(T struc)
			where T : struct
		{
			int objsize = Marshal.SizeOf(typeof(T));
			Byte[] ret = new Byte[objsize];
			IntPtr buff = Marshal.AllocHGlobal(objsize);
			Marshal.StructureToPtr(struc, buff, true);
			Marshal.Copy(buff, ret, 0, objsize);
			Marshal.FreeHGlobal(buff);
 
			Write(ret.Length);
			Write(ret);
		}
	}
}

